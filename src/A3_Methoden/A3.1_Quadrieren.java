import java.util.Scanner;

public class Quadrieren {
   
	public static void main(String[] args) {


		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		title();

		double ergebnis = verarbeitung(val1);
				
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// verarbeitung(val1);
		// ================================
		body(val1, ergebnis);
		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
	}	

	public static void double eingabe(double x) {
		Scanner tastatur = new Scanner(System.in);
		double val1;
		System.out.print("Geben Sie ein x Wert ein: ");
        val1 = tastatur.nextDouble();
	}



	public static void double verarbeitung(double x) {
		double ergebnis = x * x;
		return ergebnis;
	}


	public static void title() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
	}

	public static void body(double x, double xsquared) {
		System.out.printf("x = %.2f und x�= %.2f\n", x, xsquared);
	}

}
