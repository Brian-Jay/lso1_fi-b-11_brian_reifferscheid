
/* Das Programm startet mit der Geschwindigkeit v = 0.0 und ändert die Geschwindigkeit entsprechend den 
vom Benutzer eingegebenen Werten dv(positiver Wert heißt beschleunigen, negativer Wert heißt bremsen). 
 Aufruf z. B.: v = beschleunige(v, dv). 
Die Methode soll dafür sorgen, dass die Höchstgeschwindigkeit 130 km/h beträgt 
und die kleinste Geschwindigkeit 0 km/h. Die neue Geschwindigkeit wird jeweils ausgegeben.
**/

 class Fahrsimulator {

	public static void main(String[] args) {

        double geschwindigkeit = beschleunige(30.4, 15.3);
        System.out.printf("Geschwindigkeit: %.2f ", geschwindigkeit);

    	
	}

    // v = Geschwindigkeit
    // dv = Beschleunigung
    // t = Time
    //v = a · t + v0

    public static double beschleunige(double dv, double time) {

        int v;
        String errorMessage;
        v = 0;

        double finalV = dv * time + v;
        
        if(finalV <= 130 || finalV == 0) {
            return finalV;
        } else {
            return 0;
        }
    }

}
   
