import java.lang.Math;


/* Erstellen Sie in Ihrer Klasse Mathe die statische Methode double hypotenuse(double kathete1, double kathete2). Die Methode hypotenuse berechnet die Länge der Hypotenuse eines
rechtwinkligen Dreiecks aus den Längen der beiden Katheten: h = k 2 + k 2 . Zur Berechnung der 12
Quadrate der beiden Kathetenlängen verwenden Sie Ihre Methode quadrat (s. o.). Zur Berechnung der Wurzel verwenden Sie die Methode Math.sqrt aus der Bibliothek.
Die Berechnung der Hypotenusenlänge in der Methode hypotenuse könnte damit wie folgt aussehen:
h = Math.sqrt(quadrat(k1) + quadrat(k2));
Die Berechnung der Hypotenusenlänge im Hauptprogramm erfolgt durch den Aufruf
h = Mathe.hypotenuse(k1, k2);
**/

 class Mathe {

	public static void main(String[] args) {
        
        double hypt = hypotenuse(5.4,4.3);

        System.out.printf("Hypotenusenlänge: %.2f", hypt);
        
	}

   

    public static double quadrat(double x) {
        return x * x;
    }

    public static double hypotenuse(double kathete1, double kathete2) {
        return Math.sqrt(quadrat(kathete1) + quadrat(kathete2));

    }

}
   
