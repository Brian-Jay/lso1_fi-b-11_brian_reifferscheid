import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
        String artikel = liesString(myScanner);
        int amount = liesInt(myScanner);
        double preis = liesDouble(myScanner);
        double nettogesamtpreis = berechneGesamtnettopreis(amount, preis);
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();
        // Bruttogesgesamtpreis
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
        
        rechnungausgeben(artikel, amount, bruttogesamtpreis ,nettogesamtpreis, mwst);
       


	}

    // Artikel
    public static String liesString(Scanner myScanner) {
        System.out.println("was m�chten Sie bestellen?");
        return myScanner.next();
    }

    // Ammount
    public static int liesInt(Scanner myScanner) {
        System.out.println("Geben Sie die Anzahl ein:");
		return myScanner.nextInt();
    }

    // Preis
    public static double liesDouble(Scanner myScanner) {
        System.out.println("Geben Sie den Nettopreis ein:");
		return myScanner.nextDouble();
    }

    // Netto
    public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
        return anzahl * nettopreis;
    }

    // Brutto
    public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
        return nettogesamtpreis * (1 + mwst / 100);
    }

     public static void rechnungausgeben(String artikel, int anzahl, double  nettogesamtpreis, double bruttogesamtpreis, double mwst) {
          // Rechnung
       	System.out.printf("\t\t Rechnung: %-15s %6d %10.2f %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis ,nettogesamtpreis, mwst, "%");
     }

 

    
    



}