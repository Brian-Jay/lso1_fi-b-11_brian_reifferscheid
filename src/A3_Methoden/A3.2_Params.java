

public class Schaltungen {

	public static void main(String[] args) {

    	double ersatzwiderstand = reihenschaltung(3.5, 2.3);
    	double parallelschaltung = parallelschaltung(4.5, 5.3);
        double quadrat = quadrat(4);

        System.out.printf("Ersatzwiderstand: %.2f \n", ersatzwiderstand);
        System.out.printf("Ersatzwiderstand: %.2f \n", parallelschaltung);
        System.out.printf("Ersatzwiderstand: %.2f \n", quadrat);
	}

    //Aufgabe 3

    // Erstellen Sie die statische Methode double reihenschaltung(double r1, double r2). Die Methode reihenschaltung soll den Ersatzwiderstand der Reihenschaltung aus r1 und r2 berechnen.

    public static double reihenschaltung(double r1, double r2) {
        return r1 + r2;
    }

    // Aufagbe 4
    // Erstellen Sie die statische Methode double parallelschaltung(double r1, double r2). Die Methode parallelschaltung soll den Ersatzwiderstand der Parallelschaltung aus r1 und r2 berechnen.
    public static double parallelschaltung(double r1, double r2) {
        return r1 + r2;
    }

    // Aufgabe 5 
    // Erstellen Sie die Klasse Mathe und darin die statische Methode double quadrat(double x). Die Methode quadrat soll das Quadrat des Parameters x als Rückgabewert liefern.
    public static double quadrat(double x) {
        return x * x;
    }





}