// EVA-Principle Eingabe, Verarbeitung und Ausageb 
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
        double singlePrice = fahrkartenBetragErfassen(tastatur);
        int tickets = fahrkartenTicketsErfassen(tastatur);
        double gesamtBetrag = totalPrice(singlePrice, tickets);
        double EingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur ,gesamtBetrag);
        warte();
        double totalPreis = totalPrice(singlePrice, tickets);
        // int puffer = warte(250);
        double rueckgeldAusgeben = rueckgeldAusgeben(totalPreis, EingezahlterGesamtbetrag);

    
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }


    public static double fahrkartenBetragErfassen(Scanner tastatur) {
        double singlePrice;
        // Eingabe
        System.out.print("Zu zahlender Betrag (EURO): ");
        singlePrice = tastatur.nextDouble();
        // Ausgabe 
        return singlePrice;
       
    }
    public static int fahrkartenTicketsErfassen(Scanner tastatur) {
        int tickets;
        // Eingabe
        System.out.print("Ticketmenge: ");
        tickets = tastatur.nextInt();
        // Ausgabe 
        return tickets;
    }

// EingezahlterGesamtbetrag
    public static double fahrkartenBezahlen(Scanner tastatur, double totalPreis) {
        // Geldeinwurf
       // -----------
        double eingezahlterGesamtbetrag;    
       eingezahlterGesamtbetrag = 0.0;
       double eingeworfeneMünze;
    //    double totalPreis;


       while(eingezahlterGesamtbetrag < totalPreis)
       {
            // Eingabe
    	   System.out.printf("Ticket Preis: %.2f ", totalPreis);
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
            // Verarbeitung    
           eingezahlterGesamtbetrag += eingeworfeneMünze;

       }
    //  Ausgabe
           return eingezahlterGesamtbetrag;
    }



    // Gesamtbetrag
    public static double totalPrice(double singlePrice, int tickets) {
        // Verarbeitung und Ausgabe
        return singlePrice * tickets;
    }



    public static void warte() {
         // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
    }

    public static double rueckgeldAusgeben(double totalPreis, double eingezahlterGesamtbetrag) {
        // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       double rückgabebetrag;
       rückgabebetrag = eingezahlterGesamtbetrag - totalPreis;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von ");
    	   System.out.printf("%.2f\n", rückgabebetrag);
           System.out.print(" EURO");
    	   System.out.println(" wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }
       return rückgabebetrag;
    }
}










