import java.util.Scanner;

class Switch
{
    public static void main(String[] args)
    {
       Scanner eingabe = new Scanner(System.in);
        double R, U, I;
        String input;
        System.out.print("Wählen Sie die Größe aus, die Sie berechnen wollen? ")
        input = eingabe.next();
        
        switch (input) {
            case "R":
                System.out.println("Eingabe Spannung U : ");
                U = eingabe.nextDouble();
                System.out.println("Eingabe Stromstärke I : ");
                I = eingabe.nextDouble();
                R = U / I;
                System.out.print("Widerstand = ");
                System.out.print(R);
                System.out.print(" Ohm");
                break;
            case "U":
                System.out.println("Eingabe Widerstand R: ");
                R = eingabe.nextDouble();
                System.out.println("Eingabe Stromstärke I : ");
                I = eingabe.nextDouble();
                U = R * I;
                System.out.print("Spannung = ");
                System.out.print(U);
                System.out.print(" Volt");
                break;
            case "I":
                System.out.println("Eingabe Widerstand U: ");
                U = eingabe.nextDouble();
                System.out.println("Eingabe Stromstärke R: ");
                R = eingabe.nextDouble();
                I = U / R;
                System.out.print("Stromstärke = ");
                System.out.print(I);
                System.out.print(" Amper");
                break;
            default:
                System.out.println("Keine Valide Eingabe");
                break;
        }
        eingabe.close();
    }
}