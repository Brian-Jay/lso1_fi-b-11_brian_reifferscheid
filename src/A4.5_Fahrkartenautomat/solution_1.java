// Der Fahrkartenautomat dahingehend verbessert werden, so dass der Automat nur einen Wert zwischen 1 bis 10 für die Anzahl 
//der Tickets annimmt. Gibt der Benutzer einen ungültigen Wert ein, soll das Programm mit dem Standardwert (anzahl =j) 
//weitermachen und dies dem Benutzer durch eine aussagekräftige Fehlermeldung mitteilen. Dies soll ebenso bei negativen 
//Ticketpreisen gelten. 
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {

        Scanner tastatur = new Scanner(System.in);
        boolean endlos = true;
        while(endlos) {
        double singlePrice = fahrkartenBetragErfassen(tastatur);
        int tickets = fahrkartenTicketsErfassen(tastatur);
        double gesamtBetrag = totalPrice(singlePrice, tickets);
        double EingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur ,gesamtBetrag);
        warte(150);
        double totalPreis = totalPrice(singlePrice, tickets);
        double rueckgeldAusgeben = rueckgeldAusgeben(totalPreis, EingezahlterGesamtbetrag);
        double coinUnit = muenzeAusgeben(rueckgeldAusgeben, " Euro");
        }
    
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }


    public static double fahrkartenBetragErfassen(Scanner tastatur) {
        
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
        System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (0)");
        System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (1)");
        System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (2)");
        System.out.println("Wählen sie eine Nummer aus:");
        double selectedTicket;
        int index = tastatur.nextInt();
        double[] ticketTypes = {2.90, 8.60, 23.50};
        if(index > 2) {
            System.out.println("Eingabe ist nicht valide");
        } 
        selectedTicket = ticketTypes[index];
        
        
        // System.out.println(ticketTypes[index]);
        
        // Eingabe
        System.out.print("Zu zahlender Betrag (EURO): ");
        System.out.println(selectedTicket);
         
        return selectedTicket;
       
    }
    public static int fahrkartenTicketsErfassen(Scanner tastatur) {
        int tickets;
        int output;
        // Eingabe
        System.out.print("Ticketmenge: ");
        tickets = tastatur.nextInt();
        if(tickets < 11) {
             output = tickets;
        } else {
            System.out.println("Please enter a number betweeen 1-10!");
            output = 1;
        }
        // Ausgabe 
        return output;
    }

// EingezahlterGesamtbetrag
    public static double fahrkartenBezahlen(Scanner tastatur, double totalPreis) {
        // Geldeinwurf
       // -----------
        double eingezahlterGesamtbetrag;    
       eingezahlterGesamtbetrag = 0.0;
       double eingeworfeneMünze;
    //    double totalPreis;


       while(eingezahlterGesamtbetrag < totalPreis)
       {
            // Eingabe
    	   System.out.printf("Ticket Preis: %.2f ", totalPreis);
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
            // Verarbeitung    
           eingezahlterGesamtbetrag += eingeworfeneMünze;

       }
    //  Ausgabe
           return eingezahlterGesamtbetrag;
    }



    // Gesamtbetrag
    public static double totalPrice(double singlePrice, int tickets) {
        // Verarbeitung und Ausgabe
        return singlePrice * tickets;
    }



    public static void warte(int milisec) {
         // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(milisec);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
    }

    public static double rueckgeldAusgeben(double totalPreis, double eingezahlterGesamtbetrag) {
        // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       double rückgabebetrag;
       rückgabebetrag = eingezahlterGesamtbetrag - totalPreis;
       return rückgabebetrag;
       
}

public static double muenzeAusgeben(double betrag, String einheit) {
    if(betrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von ");
    	   System.out.printf("%.2f\n", betrag);
           System.out.print(einheit);
    	   System.out.println(" wird in folgenden Münzen ausgezahlt:");

           while(betrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          betrag -= 2.0;
           }
           while(betrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          betrag -= 1.0;
           }
           while(betrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          betrag -= 0.5;
           }
           while(betrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          betrag -= 0.2;
           }
           while(betrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          betrag -= 0.1;
           }
           while(betrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          betrag -= 0.05;
           }
       }
       return betrag;
    }

}








