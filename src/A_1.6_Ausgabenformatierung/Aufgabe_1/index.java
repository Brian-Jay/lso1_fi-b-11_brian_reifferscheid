public class aufgabe1 {
    public static void main(String[] args) {

        String starOne = "**";
        String starTwo = "*\t*\n";                                                              
        String starThree = "**\n";
        String starFour = "*\t*\n";

        System.out.printf("%21s", starOne);
        System.out.println();
        System.out.println();
        System.out.printf("%19s", starFour);
        System.out.printf("%19s", starTwo);
        System.out.println();
        System.out.printf("%22.5s", starThree);  
    }
}