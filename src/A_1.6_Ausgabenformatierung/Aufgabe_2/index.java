public class aufgabe2 {
    public static void main(String[] args) {


        String negetaNull = "0!";
        String negetaOne = "1!";
        String negetaTwo = "2!";
        String negetaThree = "3!";
        String negetaFour = "4!";
        String negetaFive = "5!";
        String equal = "=";
        String oneOutcome = "1\n";
        String one = "1";
        String twoOutcome = "2\n";
        String two = "2";
        String three = "3";
        String four = "4";
        String five = "5";
        String sixOutcome = "6\n";
        String twentyfourOutcome = "24\n";
        String hundredtwentyOutcome = "120\n";
        String multiply = "*";

        // Null
        System.out.printf(negetaNull);
        System.out.printf("%2s",equal);
        System.out.printf("%22s",equal);
        System.out.printf("%5s",oneOutcome);


        // // One
        System.out.printf(negetaOne);
        System.out.printf("%2s",equal);
        System.out.printf("%2s",one);
        System.out.printf("%20s",equal);
        System.out.printf("%5s",oneOutcome);

        // // Two
        System.out.printf(negetaTwo);
        System.out.printf("%2s",equal);
        System.out.printf("%2s",one);
        System.out.printf("%2s",multiply);
        System.out.printf("%2s",two);
        System.out.printf("%16s",equal);
        System.out.printf("%5s",twoOutcome);

        // // Three
        System.out.printf(negetaThree);
        System.out.printf("%2s",equal);
        System.out.printf("%2s",one);
        System.out.printf("%2s",multiply);
        System.out.printf("%2s",two);
        System.out.printf("%2s",multiply);
        System.out.printf("%2s",three);
        System.out.printf("%12s",equal);
        System.out.printf("%5s",sixOutcome);

        // // Four
        System.out.printf(negetaFour);
        System.out.printf("%2s",equal);
        System.out.printf("%2s",one);
        System.out.printf("%2s",multiply);
        System.out.printf("%2s",two);
        System.out.printf("%2s",multiply);
        System.out.printf("%2s",three);
        System.out.printf("%2s",multiply);
        System.out.printf("%2s",four);
        System.out.printf("%8s",equal);
        System.out.printf("%5s",twentyfourOutcome);

        // // Five
        System.out.printf(negetaFive);
        System.out.printf("%2s",equal);
        System.out.printf("%2s",one);
        System.out.printf("%2s",multiply);
        System.out.printf("%2s",two);
        System.out.printf("%2s",multiply);
        System.out.printf("%2s",three);
        System.out.printf("%2s",multiply);
        System.out.printf("%2s",four);
        System.out.printf("%2s",multiply);
        System.out.printf("%2s",five);
        System.out.printf("%4s",equal);
        System.out.printf("%5s",hundredtwentyOutcome);
    }
}