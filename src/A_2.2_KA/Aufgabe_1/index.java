public class aufgabe1 {
    public static void main(String[] args) {
    String s1 = "Das ist ein Beispielsatz.";
    String s2 = "Ein Beispielsatz ist das.";
    String space = " ";
    String completeSentence = space + s1 + space + s2;
    // Die Funktion println(); gibt den Output mit Zeilenumbruch in die Konsole aus.
    System.out.println(completeSentence);
    // Die Funktion print(); gibt den Output ohne Zeilenumbruch in die Konsole aus.
    System.out.print(completeSentence);
    }
}