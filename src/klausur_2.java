import java.util.Scanner;

public class Multplikation {

    public static void main(String[] args) {
       
        double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;

        

        Scanner myScanner = new Scanner(System.in);
       
        Programmhinweis();
        double eingabe1 = Eingabe1(zahl1, myScanner);
        double eingabe2 = Eingabe2(zahl2, myScanner);
        double ergebnis = Verarbeitung(erg ,eingabe1, eingabe2);
        double ausgabe = Ausgabe(ergebnis, eingabe1, eingabe2);
   
    }

    public static void Programmhinweis() {
        //1.Programmhinweis
        System.out.println("Hinweis: ");
        System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
    }

    public static double Eingabe1(double zahl1, Scanner myScanner) {
        //4.Eingabe
        System.out.print(" 1. Zahl: ");
        zahl1 = myScanner.nextDouble();
        return zahl1;
    }
    public static double Eingabe2(double zahl2, Scanner myScanner) {
        //4.Eingabe
        System.out.print(" 2. Zahl: ");
        zahl2 = myScanner.nextDouble();
        return zahl2;
    }

    public static double Verarbeitung(double erg, double zahl1, double zahl2) {
        //3.Verarbeitung
        return erg = zahl1 * zahl2;
    }

    public static double Ausgabe(double erg, double zahl1, double zahl2) {
        //2.Ausgabe   
        System.out.println("Ergebnis der Multiplikation: ");
        System.out.printf("%.2f * %.2f = %.2f%n", zahl1, zahl2, erg);
        return erg;
    }
}