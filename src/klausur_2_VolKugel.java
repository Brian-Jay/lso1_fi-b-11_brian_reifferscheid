import java.util.Scanner;

public class Volumen {

    public static void main(String[] args) {
       
        Scanner myScanner = new Scanner(System.in);
        double pi = 3.14;

        double radiusEingabe = radius(myScanner);
        double potenz = potenceOfRadius(radiusEingabe);
        double volumen = VolumenKugel(pi, potenz);
       
    }

    public static double radius(Scanner myScanner) {
        System.out.println("Wert für den Radius eingeben: ");
        double radius = myScanner.nextDouble();
        return radius;
    }

    public static double potenceOfRadius(double radius) {
        return radius * radius * radius;
    }

    public static double VolumenKugel(double pi, double radius) {
        //2.Ausgabe   
        double bruch = 4 / 3;
        System.out.println("Ergebnis des Volumens der Kugel: ");
        System.out.printf("%.2f * %.2f = %.2f%n", bruch, pi, radius);
        return bruch * pi * radius;
    }


   
}