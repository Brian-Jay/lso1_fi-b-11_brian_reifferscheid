import java.util.Scanner;

public class MittelwertMitMethoden {

	public static void main(String[] args) {
		double zahl;
        double summe = 0;
		// double zahl2;
		double m;
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Die anzahl der angaben eingeben:");
        int anzahl = myScanner.nextInt();
		System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
        for(int i = 1; i <= anzahl; i++) {
		zahl = eingabe(myScanner, "Bitte geben Sie die erste Zahl ein: ");
        summe = summe + zahl;
        }
        m = mittelwertBerechnung(summe, anzahl);
        ausgabe(m);
		
		// zahl2 = eingabe(myScanner, "Bitte geben Sie die zweite Zahl ein: ");
		
		
		myScanner.close();
	}
	
	public static double eingabe(Scanner ms, String text) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}
	
	public static double mittelwertBerechnung(double summe, double anzahl) {
		double m = summe / anzahl;
		return m;
	}

	
	public static void ausgabe(double mittelwert) {
		System.out.println("Der errechnete Mittelwert aller Zahlen ist: " + mittelwert);
	}
}
